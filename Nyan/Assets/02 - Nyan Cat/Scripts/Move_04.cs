﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Goldsmiths.MACGAD.NyanCat
{
    public class Move_04 : MonoBehaviour
    {
        [Range(0, 50)]
        [Tooltip("Moving speed, in metres/second")]
        public float Speed = 0.5f;

        // Update is called once per frame
        void Update()
        {
            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");

            if (x != 0 || y != 0)
                transform.position += new Vector3(x, y) * Speed * Time.deltaTime;
        }
    }
}