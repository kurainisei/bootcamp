﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Goldsmiths.MACGAD.NyanCat
{
    public class Move_01 : MonoBehaviour
    {
        [Range(0, 5)]
        [Tooltip("Moving speed, in metres/second")]
        public float Speed = 0.5f;

        // Update is called once per frame
        void Update()
        {
            // Horizontal
            if (Input.GetKey(KeyCode.RightArrow))
                transform.position += Vector3.right * Speed * Time.deltaTime;
            if (Input.GetKey(KeyCode.LeftArrow))
                transform.position += Vector3.left * Speed * Time.deltaTime;

            // Vertical axis
            if (Input.GetKey(KeyCode.UpArrow))
                transform.position += Vector3.up * Speed * Time.deltaTime;
            if (Input.GetKey(KeyCode.DownArrow))
                transform.position += Vector3.down * Speed * Time.deltaTime;
        }
    }
}