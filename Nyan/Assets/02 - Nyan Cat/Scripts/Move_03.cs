﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Goldsmiths.MACGAD.NyanCat
{
    public class Move_03 : MonoBehaviour
    {
        [Range(0, 5)]
        [Tooltip("Moving speed, in metres/second")]
        public float Speed = 0.5f;

        // Update is called once per frame
        void Update()
        {
            // Horizontal axis
            float x = Input.GetAxis("Horizontal");
            if (x != 0)
                transform.position += Vector3.right * x * Speed * Time.deltaTime;

            // Vertical axis
            float y = Input.GetAxis("Vertical");
            if (y != 0)
                transform.position += Vector3.up * y * Speed * Time.deltaTime;
        }
    }
}