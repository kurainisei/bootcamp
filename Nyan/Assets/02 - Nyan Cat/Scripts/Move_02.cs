﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Goldsmiths.MACGAD.NyanCat
{
    public class Move_02 : MonoBehaviour
    {
        [Range(0, 5)]
        [Tooltip("Moving speed, in metres/second")]
        public float Speed = 0.5f;

        [Header("Controls")]
        public KeyCode RightKey = KeyCode.RightArrow;
        public KeyCode LeftKey  = KeyCode.LeftArrow;
        public KeyCode UpKey    = KeyCode.UpArrow;
        public KeyCode DownKey  = KeyCode.DownArrow;

        // Update is called once per frame
        void Update()
        {
            // Horizontal
            if (Input.GetKey(RightKey))
                transform.position += Vector3.right * Speed * Time.deltaTime;
            if (Input.GetKey(LeftKey))
                transform.position += Vector3.left * Speed * Time.deltaTime;

            // Vertical axis
            if (Input.GetKey(UpKey))
                transform.position += Vector3.up * Speed * Time.deltaTime;
            if (Input.GetKey(DownKey))
                transform.position += Vector3.down * Speed * Time.deltaTime;
        }
    }
}