﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Rigidbody2D))]
public class PipeBehavior : MonoBehaviour
{
    public float speed;
    public ScoreManager scoreManager;
    Rigidbody2D _rb;
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _rb.velocity = -Vector2.right * speed; 
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x<-20)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            scoreManager.UpdateScore();
            Debug.Log("Passed Obstacle");
        }

    }
}
