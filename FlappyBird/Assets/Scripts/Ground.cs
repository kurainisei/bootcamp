﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour
{
    public float speed;
    private Vector2 _rbSpeed;
    private float _width;
    private Rigidbody2D _rb;
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _width = GetComponent<SpriteRenderer>().size.x;
        Debug.Log(_width);
        _rbSpeed = new Vector2(-speed, 0);
        _rb.velocity = _rbSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x<=-_width)
        {
            transform.position = new Vector2 
                (transform.position.x+_width*2,transform.position.y);
        }
    }
}
