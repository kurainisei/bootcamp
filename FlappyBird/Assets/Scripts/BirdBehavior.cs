﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Rigidbody2D))]

public class BirdBehavior : MonoBehaviour
{
    public ScoreManager scoreManager;
    Rigidbody2D _rb;
    public float force;
    public float velocityCap;
    public float heightCap;
    Animator _anim;
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)
            &&_rb.velocity.y<velocityCap
            &&transform.position.y<heightCap)
        {
            _rb.AddForce(transform.up * force);
            _anim.SetTrigger("Flap");
        }
        //Debug.Log(transform.position.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Game Over");
        scoreManager.GameOver();
    }

}
