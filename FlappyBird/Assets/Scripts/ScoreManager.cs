﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour
{
    private int _score;
    public UIManager ui;
    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.SetInt("score", 0);
        _score = 0;
    }

    public void UpdateScore()
    {
        _score++;
        ui.UpdateScore(_score);
        Debug.Log(_score);
    }

    public void GameOver()
    {
        PlayerPrefs.SetInt("score", _score);
        SceneManager.LoadScene(2);
    }
}
