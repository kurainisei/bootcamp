﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeSpawner : MonoBehaviour
{
    public float maxY=3;
    public float minY=-2;
    public GameObject pipe;
    public float spawnRate;
    public ScoreManager scoreManager;

    private float _timer;
    // Start is called before the first frame update
    void Start()
    {
          
    }

    // Update is called once per frame
    void Update()
    {
        if (_timer>=spawnRate)
        {
            //Spawn
            float offset = Random.Range(minY, maxY);
            Vector3 spawnPosition = new Vector3
                (transform.position.x, transform.position.y + offset, transform.position.z);
            GameObject currentPipe = GameObject.Instantiate(pipe, spawnPosition, Quaternion.identity);
            currentPipe.GetComponent<PipeBehavior>().scoreManager = scoreManager;
            //Reset timer
            _timer -= spawnRate;
        }
        //Update timer
        _timer += Time.deltaTime;
    }
}
