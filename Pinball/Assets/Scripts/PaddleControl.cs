﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleControl : MonoBehaviour
{
    public float speed;
    private HingeJoint2D _hinge;
    private JointMotor2D _motor;

    private bool _isKeyPressed;
    // Start is called before the first frame update
    void Start()
    {
        _hinge = GetComponent<HingeJoint2D>();
        _motor = _hinge.motor;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            _isKeyPressed = true;
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            _isKeyPressed = false;
        }
        //Debug.Log(_isKeyPressed);

    }

    void FixedUpdate()
    {
        if (_isKeyPressed)
        {
            _motor.motorSpeed = speed;
            
        }

        else
        {
            _motor.motorSpeed = -speed;

        }
        Debug.Log(Time.fixedDeltaTime);
        _hinge.motor = _motor;
    }
}
