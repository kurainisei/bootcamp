﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiamondLight : MonoBehaviour
{
    public Color offColor;
    public Color onColor;
    bool _isLit;
    SpriteRenderer _sprite;
    // Start is called before the first frame update
    void Start()
    {
        _isLit = false;
        _sprite = GetComponent<SpriteRenderer>();
        _sprite.color = offColor;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Ball") && !_isLit)
        {
            _sprite.color = onColor;
            _isLit = true;
        }
    }
}
