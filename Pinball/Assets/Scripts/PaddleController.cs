﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour
{
    public float PaddleSpeed;
    public bool isLeftPaddle;
    private HingeJoint2D _hingeJoint;
    private JointMotor2D _motor2D;
    private bool _isKeyPress;
    private KeyCode _key;
    private int _motorMultiplier;
 
    // Start is called before the first frame update
    void Start()
    {
        _hingeJoint = GetComponent<HingeJoint2D>();
        _motor2D = _hingeJoint.motor;
        _key = isLeftPaddle ? KeyCode.LeftArrow : KeyCode.RightArrow;
        _motorMultiplier = isLeftPaddle ? 1 : -1;
    }

    void Update()
    {
        if (Input.GetKeyDown(_key))
        {
            _isKeyPress = true;
        }
        if (Input.GetKeyUp(_key))
        {
            _isKeyPress = false;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (_isKeyPress)
        {
            _motor2D.motorSpeed = _motorMultiplier*PaddleSpeed;
            _hingeJoint.motor = _motor2D;
        }
        else 
        {
            _motor2D.motorSpeed = -_motorMultiplier*PaddleSpeed;
            _hingeJoint.motor = _motor2D;
        }
    }
}
