﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text text;
    public Transform[] collectibles;
    public int score;
    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        for (int i=0; i<collectibles.Length; i++)
        {
            collectibles[i].GetComponent<TriggerCollision>().scoreManager = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        text.text = score.ToString();
    }
}
