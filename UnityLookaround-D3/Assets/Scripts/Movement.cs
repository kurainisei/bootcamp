﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Horizontal")!=0||Input.GetAxis("Vertical")!=0)
        {
            transform.position = transform.position + Vector3.right * Input.GetAxis("Horizontal") * speed * Time.deltaTime;
            transform.position = transform.position + Vector3.up * Input.GetAxis("Vertical") * speed * Time.deltaTime;
        }
    }

    /// <summary>
    /// Prints speed on the console
    /// </summary>
    public void DebugSpeed()
    {
        Debug.Log(speed);
    }
}
