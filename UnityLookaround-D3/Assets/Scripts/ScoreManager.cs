﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text text;
    public GameObject collectiblePrefab;
    public Transform[] collectibles;
    public int score;
    float _timer;
    float _waitingTime;
    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        _timer = 0;
        _waitingTime = 2;
    }

    // Update is called once per frame
    void Update()
    {
        _timer += Time.deltaTime;
        if(_timer>=_waitingTime)
        {
            SpawnCollectible();
            _timer = _timer-_waitingTime;
        }
        text.text = score.ToString();
    }

    void SpawnCollectible()
    {
        Vector3 randomScreenPos = new Vector3
            (Random.Range(0, Camera.main.pixelWidth),
            Random.Range(0, Camera.main.pixelHeight), 0);

        Vector2 randomWorldPos = Camera.main.ScreenToWorldPoint(randomScreenPos);
        //Debug.Log(randomWorldPos);
        GameObject coll = GameObject.Instantiate
            (collectiblePrefab, randomWorldPos, transform.rotation);
        coll.GetComponent<TriggerCollision>().scoreManager = this;
    }
}
