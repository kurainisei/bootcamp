﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerCollision : MonoBehaviour
{
    public ScoreManager scoreManager;
    ParticleSystem particles;

    private void Start()
    {
        particles = GameObject.Find("CollectibleParticles")
            .GetComponent<ParticleSystem>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        particles.transform.position = transform.position;
        particles.Play();
        scoreManager.score++;
        Destroy(this.gameObject);
      //  Debug.Log("Enter");
    }

    private void OnTriggerStay2D  (Collider2D collider)
    {
       // Debug.Log("Stay");
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
      //  Debug.Log("Exit");
    }
}
