﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wobble : MonoBehaviour 
{
    public float amplitudeX;
    public float frequencyX;
    public float amplitudeY;
    public float frequencyY;
    public float offset;
    private float _wobbleX;
    private float _wobbleY;
    private Vector3 _scaleFactor;
    // Start is called before the first frame update
    void Start()
    {
        _scaleFactor = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        _wobbleX = offset + Mathf.Cos(Time.time*frequencyX)*amplitudeX;
        _wobbleY = offset + Mathf.Sin(Time.time * frequencyY) * amplitudeY;
        _scaleFactor.x = _wobbleX;
        _scaleFactor.y = _wobbleY;
        transform.localScale = _scaleFactor;
    }
}
