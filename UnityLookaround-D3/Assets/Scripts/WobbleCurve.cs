﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WobbleCurve : MonoBehaviour
{
    public AnimationCurve wobbleXCurve;
    public AnimationCurve wobbleYCurve;
    public Gradient collectibleColor;
    private Vector3 _currentScale;
    private float _timeOffset;
    private SpriteRenderer _spriteRend;
    // Start is called before the first frame update
    void Start()
    {
        _spriteRend = GetComponent<SpriteRenderer>();
        _currentScale = transform.localScale;
        _timeOffset = Random.value;
    }

    // Update is called once per frame
    void Update()
    {
        float periodicvalue = 0.5f+ Mathf.Sin(Time.time + _timeOffset) * 0.5f;
        Debug.Log(periodicvalue);
        _spriteRend.color = collectibleColor.Evaluate
            (periodicvalue);
        _currentScale.x = wobbleXCurve.Evaluate(Time.time+_timeOffset);
        _currentScale.y = wobbleYCurve.Evaluate(Time.time+_timeOffset);
        transform.localScale = _currentScale;
    }
}
