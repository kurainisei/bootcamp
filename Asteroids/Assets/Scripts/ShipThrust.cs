﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Rigidbody2D))]
public class ShipThrust : MonoBehaviour
{
    public KeyCode key = KeyCode.UpArrow;
    public float speed;
    public Sprite thrustOn;
    public Sprite thrustOff;

    Rigidbody2D _rb;
    SpriteRenderer _sr;
    int _thrustState = 0;
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(key))
        {
            //activate thrust
            _thrustState = 1;
            _sr.sprite = thrustOn;
        }
        else if (Input.GetKeyUp(key))
        {
            //deactivateThrust
            _thrustState = 0;
            _sr.sprite = thrustOff;
        }
    }

    private void FixedUpdate()
    {
        _rb.AddForce(transform.up * speed * _thrustState);
    }

}
