﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawner : MonoBehaviour
{
    public GameManager gameManager;
    public Transform asteroid;
    public Transform spawner;
    public float spawnRate = 1;

    private Vector2 _screenBounds;
    private float _angle;
    private float _timer;
    // Start is called before the first frame update
    void Start()
    {
        _screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        for (int i=0; i<10; i++)
        {
            SpawnAsteroid();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_timer>=spawnRate)
        {
            SpawnAsteroid();
            _timer -= spawnRate;
        }
        else
        {
            _timer += Time.deltaTime;
        }
    }

    void SpawnAsteroid()
    {
        _angle = Random.Range(0f, 360f);
        spawner.transform.position = new Vector2(_screenBounds.x * Mathf.Cos(_angle), _screenBounds.y * Mathf.Sin(_angle));
        Transform a = Instantiate(asteroid, spawner.transform.position, Quaternion.identity);
        a.GetComponent<AsteroidDeath>().gm = gameManager;
    }
}
