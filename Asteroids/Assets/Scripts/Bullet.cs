﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Rigidbody2D))]
public class Bullet : MonoBehaviour
{
    public float bulletLife;
    public float bulletSpeed;

    private Rigidbody2D _rb;
    private float _timer;
    // Start is called before the first frame update
    void Start()
    {
        _timer = 0;
        _rb = GetComponent<Rigidbody2D>();
        _rb.AddForce(transform.up * bulletSpeed);
    }

    // Update is called once per frame
    void Update()
    {
        if (_timer<bulletLife)
        {
            _timer += Time.deltaTime;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
