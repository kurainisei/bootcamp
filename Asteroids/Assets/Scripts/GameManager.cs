﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int playerLives = 3;
    private int _score;
    public Text scoreTextBox;
    public Text livesTextBox;
    private AudioSource _audio;
    private float _timer;
    private float _interval;
    // Start is called before the first frame update
    void Start()
    {
        _audio = GetComponent<AudioSource>();
        _interval = 1f;
        _score = 0;
        livesTextBox.text = "Lives: " + playerLives;
    }

    // Update is called once per frame
    void Update()
    {
        if (_timer>=_interval)
        {
            _audio.Play();
            _timer -= _interval;
        }
        else
        {
            _timer += Time.deltaTime;
        }
    }

    public void IncreaseScore (int scoreIncrease)
    {
        _score += scoreIncrease;
        scoreTextBox.text = _score.ToString();
        Debug.Log(_score);
    }

    public void DecreaseLives()
    {
        if (playerLives>0)
        {
            playerLives--;
            livesTextBox.text = "Lives: " + playerLives;
        }
        else
        {
            SceneManager.LoadScene(0);
        }
    }
}
