﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldWrap : MonoBehaviour
{
    public Rect area;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x > area.xMax)
        {
            Vector3 position = transform.position;
            position.x = area.xMin;
            transform.position = position;
        }

        if (transform.position.x < area.xMin)
        {
            Vector3 position = transform.position;
            position.x = area.xMax;
            transform.position = position;
        }

        if (transform.position.y > area.yMax)
        {
            Vector3 position = transform.position;
            position.y = area.yMin;
            transform.position = position;
        }

        if (transform.position.y < area.yMin)
        {
            Vector3 position = transform.position;
            position.y = area.yMax;
            transform.position = position;
        }
    }

    //will not appear when game runs
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(area.center, area.size);
    }
}
