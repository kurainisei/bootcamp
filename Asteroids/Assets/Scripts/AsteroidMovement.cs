﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class AsteroidMovement : MonoBehaviour
{
    public float minSpeed;
    public float maxSpeed;
    private float _asteroidSpeed;
    private Rigidbody2D _rb;
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        transform.Rotate(transform.forward * Random.Range(0f, 360f));
        _asteroidSpeed = Random.Range(minSpeed, maxSpeed);
        _rb.AddForce(transform.up * _asteroidSpeed);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
