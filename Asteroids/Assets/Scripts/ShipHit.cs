﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShipHit : MonoBehaviour
{
    public UnityEvent onShipHit;
    private Rigidbody2D _rb;
    // Start is called before the first frame update
    void Start()
    {
        _rb=GetComponent<Rigidbody2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        onShipHit.Invoke();
        Destroy(collision.gameObject);
        //Destroy(this.gameObject, 0.1f);
        _rb.velocity = Vector2.zero;
        transform.position = Vector3.zero;
    }
}
