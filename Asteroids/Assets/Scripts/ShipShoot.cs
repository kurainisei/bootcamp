﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipShoot : MonoBehaviour
{

    public Transform bullet;
    public KeyCode key = KeyCode.Space;
    private AudioSource _audio;
    // Start is called before the first frame update
    void Start()
    {
        _audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(key))
        {
            _audio.Play();
            Instantiate(bullet, transform);
        }
    }
}
