﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (Rigidbody2D))]
public class ShipRotate : MonoBehaviour
{
    [Header("Ship Rotation")]
    public float speed;
    //public float angularVelocityClamp;
    private Rigidbody2D _rb;
    private float _spin;
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        _spin = Input.GetAxis("Horizontal");
    }

    private void FixedUpdate()
    {
        _rb.AddTorque(-_spin*speed);
        //_rb.angularVelocity = Mathf.Clamp(_rb.angularVelocity, -angularVelocityClamp, angularVelocityClamp);
    }
}
