﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidDeath : MonoBehaviour
{
    public GameManager gm;
    public int score = 100;

    SpriteRenderer _sprite;
    CircleCollider2D _collider;
    ParticleSystem _particles;

    void Start()
    {
        _sprite = GetComponent<SpriteRenderer>();
        _collider = GetComponent<CircleCollider2D>();
        _particles = GetComponent<ParticleSystem>();
    }

    void OnCollisionEnter2D (Collision2D collision)
    {
        if (collision.transform.CompareTag("Bullet"))
        {
            gm.IncreaseScore(score);
        }

        _sprite.enabled = false;
        _collider.enabled = false;
        _particles.Play();

        Destroy(this.gameObject, 1);
    }
}
